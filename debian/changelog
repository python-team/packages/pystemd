pystemd (0.13.2-2) unstable; urgency=medium

  * Team upload.
  * update build dependencies:
    - python3-mock
    - python3-six
    - pkg-config
    + pkgconf

 -- Alexandre Detiste <tchet@debian.org>  Wed, 27 Mar 2024 15:19:12 +0100

pystemd (0.13.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.13.2
  * Depend on D-Bus virtual packages instead of 'dbus'

 -- Luca Boccassi <bluca@debian.org>  Sun, 25 Jun 2023 14:04:11 +0100

pystemd (0.11.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.11.0
  * Drop re-cythonize.patch, fixed upstream
  * Bump Standards-Version to 4.6.2, no changes
  * Update upstream git URL, moved to systemd org
  * Switch to debhelper-compat 13
  * Add new lxml build dependency
  * Add new pkg-config build dependency
  * gbp.conf: enable pristine-tar
  * Set Rules-Requires-Root
  * Bump d/watch version

 -- Luca Boccassi <bluca@debian.org>  Fri, 10 Feb 2023 21:18:43 +0000

pystemd (0.7.0-5) unstable; urgency=medium

  [ Stefano Rivera ]
  * Team upload.
  * Patch: Include dbusc.pxd, omitted from source tarball, to allow
    re-cythonization. Thanks Steve Langasek.
  * Re-cythonize source, fixing FTBFS with Python 3.10. (Closes: #999370)

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Stefano Rivera <stefanor@debian.org>  Fri, 19 Nov 2021 11:42:17 -0400

pystemd (0.7.0-4) unstable; urgency=medium

  * Team upload.
  * Also add python3-all to upstream-unittests test depends

 -- Scott Kitterman <scott@kitterman.com>  Mon, 23 Mar 2020 05:15:28 -0400

pystemd (0.7.0-3) unstable; urgency=medium

  * Team upload.
  * Fix autopkgtest to run reliably with all supported python3 versions
    (Closes: #954471)

 -- Scott Kitterman <scott@kitterman.com>  Sun, 22 Mar 2020 00:28:51 -0400

pystemd (0.7.0-2) unstable; urgency=medium

  * Team upload.
  [ James Clarke ]
  * Limit building to linux-any due to systemd's ties to Linux

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 15 Jan 2020 13:08:27 +0100

pystemd (0.7.0-1) unstable; urgency=medium

  * Initial release. (Closes: #911563)

 -- Alexandros Afentoulis <alexaf.dpkg@bloom.re>  Wed, 11 Dec 2019 21:48:44 +0200
